# README #

This repository contains a basic structure for an initial Angular2 app.

### What is this repository for? ###

* Angular2 tutorial

### How do I get set up? ###

* Install Nodejs
* Install dependencies with 'npm install' in project folder
* Run project with 'npm start'

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* The world